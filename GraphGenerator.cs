﻿using System;
using System.Collections;
using System.Xml.Linq;
using Aras.IOM;

namespace ArasDataModelGraph
{
   class GraphGenerator
   {
      private XDocument graphML = null;
      private Innovator inn = null;
      private static XNamespace graphmlNamespace = "http://graphml.graphdrawing.org/xmlns";

      GraphGenerator(string serverURL, string dbName, string userName, string pwd)
      {
         graphML = new XDocument();
         CreateSession(serverURL, dbName, userName, pwd);
      }

      /*
      * Description: Connect to Aras server and login as given user.
      * 
      * Arguments:
      *   serverURL - Aras Server URL
      *   dbName    - Database name
      *   userName  - User name, must have admin access
      *   pwd       - Password for the user
      * 
      */
      private bool CreateSession(string serverURL, string dbName, string userName, string pwd)
      {
         Console.WriteLine("Connecting to Aras server {0} as {1} ", serverURL, userName);
         HttpServerConnection conn = IomFactory.CreateHttpServerConnection(serverURL, dbName, userName, pwd);
         Item login_result = conn.Login();
         if (login_result.isError())
         {
            throw new Exception("Login failed");
         }
         inn = IomFactory.CreateInnovator(conn);
         Console.WriteLine("Logged in as {0}", userName);

         return true;
      }

      /*
       * Description: Create and add a node for given item or relation
       * 
       * Arguments:
       *   itemType - true if node is being added for item
       *   id       - id of the node, typically internal id of class
       *   name     - value of the node, typically item name
       *   graph    - Parent element to which node is added
       *   graphml  - root element
       * 
       */
      private void AddNode(bool itemType, string id, string name, XElement graph, XElement graphml)
      {
         XElement node = new XElement(graphmlNamespace + "node");
         node.SetAttributeValue("id", id);

         // data element
         XElement dataEle = new XElement(graphmlNamespace + "data");
         dataEle.SetAttributeValue("key", "d6");
         node.Add(dataEle);
         XNamespace yns = graphml.GetNamespaceOfPrefix("y");
         XElement shapeNode = new XElement(yns + "ShapeNode");
         dataEle.Add(shapeNode);

         // add geometry
         XElement geoNode = new XElement(yns + "Geometry");
         geoNode.SetAttributeValue("height", 60.0);
         geoNode.SetAttributeValue("width", 150.0);
         shapeNode.Add(geoNode);

         // add fill
         XElement tNode = new XElement(yns + "Fill");
         tNode.SetAttributeValue("color", (itemType ? "#FFCC00" : "#00CCFF"));
         tNode.SetAttributeValue("transparent", false);
         shapeNode.Add(tNode);

         // border style
         tNode = new XElement(yns + "BorderStyle");
         tNode.SetAttributeValue("color", "#000000");
         tNode.SetAttributeValue("raised", "false");
         tNode.SetAttributeValue("type", "line");
         tNode.SetAttributeValue("width", "1.0");
         shapeNode.Add(tNode);

         // nodelabel
         XElement nodeLbl = new XElement(yns + "NodeLabel");
         nodeLbl.SetValue(name);
         nodeLbl.SetAttributeValue("fontFamily", "Dialog");
         nodeLbl.SetAttributeValue("fontSize", "12");
         nodeLbl.SetAttributeValue("fontStyle", "plain");
         nodeLbl.SetAttributeValue("hasBackgroundColor", "false");
         nodeLbl.SetAttributeValue("hasLineColor", "false");
         nodeLbl.SetAttributeValue("height", "18.701171875");
         nodeLbl.SetAttributeValue("horizontalTextPosition", "center");
         nodeLbl.SetAttributeValue("iconTextGap", "4");
         nodeLbl.SetAttributeValue("modelName", "custom");
         nodeLbl.SetAttributeValue("textColor", "#000000");
         nodeLbl.SetAttributeValue("verticalTextPosition", "bottom");
         nodeLbl.SetAttributeValue("visible", "True");
         nodeLbl.SetAttributeValue("width", "78.021484375");
         nodeLbl.SetAttributeValue("alignment", "center");
         nodeLbl.SetAttributeValue("x", "32.4892578125");
         nodeLbl.SetAttributeValue("autoSizePolicy", "content");
         nodeLbl.SetAttributeValue("y", "22.1494140625");

         // label model and smartnode label
         XElement lModel = new XElement(yns + "LabelModel");
         tNode = new XElement(yns + "SmartNodeLabelModel");
         tNode.SetAttributeValue("distance", "4.0");
         lModel.Add(tNode);
         nodeLbl.Add(lModel);

         // ModelParameter
         lModel = new XElement(yns + "ModelParameter");
         tNode = new XElement(yns + "SmartNodeLabelModelParameter");
         tNode.SetAttributeValue("labelRatioX", "0.0");
         tNode.SetAttributeValue("labelRatioY", "0.0");
         tNode.SetAttributeValue("nodeRatioX", "0.0");
         tNode.SetAttributeValue("nodeRatioY", "0.0");
         tNode.SetAttributeValue("offsetX", "0.0");
         tNode.SetAttributeValue("offsetY", "0.0");
         tNode.SetAttributeValue("upX", "0.0");
         tNode.SetAttributeValue("upY", "-1.0");
         lModel.Add(tNode);
         nodeLbl.Add(lModel);
         shapeNode.Add(nodeLbl);

         // shape
         tNode = new XElement(yns + "Shape");
         tNode.SetAttributeValue("type", (itemType ? "rectangle" : "hexagon"));
         shapeNode.Add(tNode);
         graph.Add(node);
      }

      /*
       * Description: Create and add a Edge element
       * 
       * Arguments:
       *   source    - source id
       *   target    - target id
       *   parentEle - Parent element to which edge element is added
       *   graphml   - root element
       * 
       */
      private void AddEdgeForRel(string source, string target, XElement parentEle, XElement graphml)
      {
         XNamespace yns = graphml.GetNamespaceOfPrefix("y");
         XElement edgeNode = new XElement(graphmlNamespace + "edge");
         edgeNode.SetAttributeValue("source", source);
         edgeNode.SetAttributeValue("target", target);
         XElement dataEle = new XElement(graphmlNamespace + "data");
         dataEle.SetAttributeValue("key", "d10");
         edgeNode.Add(dataEle);

         XElement pEgde = new XElement(yns + "PolyLineEdge");
         XElement tNode = new XElement(yns + "LineStyle");
         tNode.SetAttributeValue("color", "#000000");
         tNode.SetAttributeValue("type", "line");
         tNode.SetAttributeValue("width", "1.0");
         pEgde.Add(tNode);
         tNode = new XElement(yns + "Arrows");
         tNode.SetAttributeValue("source", "none");
         tNode.SetAttributeValue("target", "standard");
         pEgde.Add(tNode);
         dataEle.Add(pEgde);
         parentEle.Add(edgeNode);
      }

      /*
       * Description: Create and add a Edge element for property to item or relation
       * 
       * Arguments:
       *   source    - source id
       *   target    - target id
       *   propName  - property name
       *   parentEle - Parent element to which edge element is added
       *   graphml   - root element
       * 
       */
      private void AddEdgeForProps(string source, string target, string propName, XElement parentEle, XElement graphml)
      {
         XNamespace yns = graphml.GetNamespaceOfPrefix("y");
         XElement edgeNode = new XElement(graphmlNamespace + "edge");
         edgeNode.SetAttributeValue("source", source);
         edgeNode.SetAttributeValue("target", target);
         XElement dataEle = new XElement(graphmlNamespace + "data");
         dataEle.SetAttributeValue("key", "d10");
         edgeNode.Add(dataEle);

         XElement pEgde = new XElement(yns + "PolyLineEdge");
         XElement tNode = new XElement(yns + "LineStyle");
         tNode.SetAttributeValue("color", "#0000FF");
         tNode.SetAttributeValue("type", "dashed");
         tNode.SetAttributeValue("width", "1.0");
         pEgde.Add(tNode);
         tNode = new XElement(yns + "Arrows");
         tNode.SetAttributeValue("source", "none");
         tNode.SetAttributeValue("target", "standard");
         pEgde.Add(tNode);

         XElement eLabel = new XElement(yns + "EdgeLabel");
         eLabel.SetAttributeValue("alignment", "center");
         eLabel.SetAttributeValue("configuration", "AutoFlippingLabel");
         eLabel.SetAttributeValue("distance", "2.0");
         eLabel.SetAttributeValue("fontFamily", "Dialog");
         eLabel.SetAttributeValue("fontSize", "12");
         eLabel.SetAttributeValue("fontStyle", "plain");
         eLabel.SetAttributeValue("hasBackgroundColor", "false");
         eLabel.SetAttributeValue("hasLineColor", "false");
         eLabel.SetAttributeValue("height", "18.70117187");
         eLabel.SetAttributeValue("horizontalTextPosition", "center");
         eLabel.SetAttributeValue("iconTextGap", "4");
         eLabel.SetAttributeValue("modelName", "custom");
         eLabel.SetAttributeValue("preferredPlacement", "anywhere");
         eLabel.SetAttributeValue("ratio", "0.5");
         eLabel.SetAttributeValue("textColor", "#000000");
         eLabel.SetAttributeValue("verticalTextPosition", "bottom");
         eLabel.SetAttributeValue("visible", "true");
         eLabel.SetAttributeValue("width", "104.0546875");
         pEgde.Add(eLabel);
         eLabel.SetValue(propName);

         XElement lModel = new XElement(yns + "ModelParameter");
         tNode = new XElement(yns + "SmartEdgeLabelModelParameter");
         tNode.SetAttributeValue("angle", "6.283185307179586");
         tNode.SetAttributeValue("distance", "59.707561268278965");
         tNode.SetAttributeValue("distanceToCenter", "true");
         tNode.SetAttributeValue("position", "right");
         tNode.SetAttributeValue("ratio", "-12.44489993553995");
         tNode.SetAttributeValue("segment", "-1");
         lModel.Add(tNode);
         eLabel.Add(lModel);

         tNode = new XElement(yns + "PreferredPlacementDescriptor");
         tNode.SetAttributeValue("angle", "0.0");
         tNode.SetAttributeValue("angleOffsetOnRightSide", "0");
         tNode.SetAttributeValue("angleReference", "absolute");
         tNode.SetAttributeValue("angleRotationOnRightSide", "co");
         tNode.SetAttributeValue("distance", "-1");
         tNode.SetAttributeValue("frozen", "true");
         tNode.SetAttributeValue("placement", "anywhere");
         tNode.SetAttributeValue("side", "anywhere");
         tNode.SetAttributeValue("sideReference", "relative_to_edge_flow");
         eLabel.Add(tNode);

         dataEle.Add(pEgde);
         parentEle.Add(edgeNode);
      }

      /*
       * Description: Main method of the class, generates graphML file for all the datamodel in aras
       * 
       */
      public bool GenerateGraph()
      {
         ArrayList propertiesAvoided = new ArrayList() { "created_by_id", "id", "config_id", "source_id", "related_id", "owned_by_id", "locked_by_id", "managed_by_id", "current_state", "team_id", "modified_by_id", "permission_id" };

         XNamespace xsi = XNamespace.Get("http://www.w3.org/2001/XMLSchema-instance");

         XElement rootEle = new XElement(graphmlNamespace + "graphml");
         rootEle.Add(new XAttribute(XNamespace.Xmlns + "xsi", xsi.NamespaceName));
         rootEle.Add(new XAttribute(XNamespace.Xmlns + "x", "http://www.yworks.com/xml/yfiles-common/markup/2.0"));
         rootEle.Add(new XAttribute(XNamespace.Xmlns + "y", "http://www.yworks.com/xml/graphml"));
         rootEle.Add(new XAttribute(XNamespace.Xmlns + "sys", "http://www.yworks.com/xml/yfiles-common/markup/primitives/2.0"));
         rootEle.Add(new XAttribute(XNamespace.Xmlns + "java", "http://www.yworks.com/xml/yfiles-common/1.0/java"));
         rootEle.Add(new XAttribute(XNamespace.Xmlns + "yed", "http://www.yworks.com/xml/yed/3"));
         rootEle.Add(new XAttribute(xsi + "schemaLocation", "http://graphml.graphdrawing.org/xmlns http://www.yworks.com/xml/schema/graphml/1.1/ygraphml.xsd"));

         // defining graphical configuration elements to visualize in yed
         XElement keyGraph = new XElement(graphmlNamespace + "key");
         keyGraph.SetAttributeValue("for", "node");
         keyGraph.SetAttributeValue("id", "d6");
         keyGraph.SetAttributeValue("yfiles.type", "nodegraphics");
         XElement keyEdgeGraph = new XElement(graphmlNamespace + "key");
         keyEdgeGraph.SetAttributeValue("for", "edge");
         keyEdgeGraph.SetAttributeValue("id", "d10");
         keyEdgeGraph.SetAttributeValue("yfiles.type", "edgegraphics");

         rootEle.Add(keyGraph);
         rootEle.Add(keyEdgeGraph);

         // add the main graph element
         XElement graphElem = new XElement(graphmlNamespace + "graph");
         graphElem.SetAttributeValue("edgedefault", "directed");
         graphElem.SetAttributeValue("id", "G");
         rootEle.Add(graphElem);

         // start browsing the datamodel
         Item allItemsTypes = inn.newItem("Itemtype", "get");
         allItemsTypes.setAttribute("select", "name");
         allItemsTypes.setProperty("is_relationship", "0");

         Item itemTypeProperty = allItemsTypes.createRelationship("Property", "get");
         itemTypeProperty.setProperty("data_type", "item");
         allItemsTypes = allItemsTypes.apply();

         // loop through itemtypes to build graph nodes
         int size = allItemsTypes.getItemCount();
         Console.WriteLine("Found item ", size);
         for (int index = 0; index < size; index++)
         {
            Item currItem = allItemsTypes.getItemByIndex(index);
            string clsName = currItem.getProperty("name", "");

            // for each non-relationship itemtype create an element
            AddNode(true, currItem.getID(), clsName, graphElem, rootEle);
         }

         // Query for the relationships
         Item allRels = inn.newItem("RelationshipType", "get");
         allRels = allRels.apply();
         size = allRels.getItemCount();
         Console.WriteLine("Found relations ", size);
         for (int index = 0; index < size; index++)
         {
            Item currRel = allRels.getItemByIndex(index);
            string source_id = currRel.getProperty("source_id", "");
            string related_id = currRel.getProperty("related_id", "");
            string relname = currRel.getProperty("name", "");
            string relation_id = currRel.getProperty("relationship_id", "");

            // create relationship Itemtype
            AddNode(false, relation_id, relname, graphElem, rootEle);

            if (source_id != "")
            {
               // create edge in
               AddEdgeForRel(source_id, relation_id, graphElem, rootEle);
            }
            if (related_id != "")
            {
               // create edge out
               AddEdgeForRel(relation_id, related_id, graphElem, rootEle);
            }
         }

         // add the item property links
         size = allItemsTypes.getItemCount();

         for (int index = 0; index < size; index++)
         {
            Item currItem = allItemsTypes.getItemByIndex(index);
            int relSize = currItem.getRelationships("Property").getItemCount();
            for (int relIndex = 0; relIndex < relSize; relIndex++)
            {
               Item currProp = currItem.getRelationships("Property").getItemByIndex(relIndex);
               if (!propertiesAvoided.Contains(currProp.getProperty("name", "")))
               {
                  if (currProp.getProperty("data_source", "") == "") continue;
                  AddEdgeForProps(currItem.getID(), currProp.getProperty("data_source", ""), currProp.getProperty("name", ""), graphElem, rootEle);
               }
            }
         }

         // saving the ouput
         graphML.Add(rootEle);
         graphML.Save("arasmodel.graphml");
         return true;
      }

      public static void PrintHelp()
      {
         Console.WriteLine("usage: ");
         Console.WriteLine("\t ArasDataModelGraph <Aras server URL> <Database name> <admin username> <password> ");
         Console.WriteLine("\t Server URL is of format http://aras.test.com/InnovatorSolutions ");
      }

      static void Main(string[] args)
      {
         if (args.Length != 4)
         {
            PrintHelp();
            return;
         }

         GraphGenerator gen = new GraphGenerator(args[0], args[1], args[2], args[3]);
         gen.GenerateGraph();
      }
   }
}
