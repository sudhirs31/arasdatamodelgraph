# Aras DataModel graphML Generator

Generate GraphML file for Aras data model. The generated graph can be viewed using viewer like yEd.

C# version of the script shared [here](http://community.aras.com/en/visualize-aras-innovator-data-model-graphml/)

This is a C# console application. Tested with dotnet framework 4.7.2 and aras version 11 sp14, sp12

## Build

- Create a new C# console project from visual studio.
- Add the cs file.
- Add external reference IOM.dll, The dll provided by Aras, contains all API's needed by the utility.

## Runnning the app

Takes 4 arguments, in order
- Server URL - Aras server url, eg: http://test.aras.com/InnovatorSolutions
- Database Name - Name of the Database in Aras
- User Name - Name of the user, must have admin access, as we need access to all datamodel
- Password - Password for the user

Generates arasmodel.graphml in the directory where the exe is located.

## Viewing the Graph

Open the graphml file in [yEd viewer](https://www.yworks.com/products/graphmlviewer).
Change the layout to Hierarchical.

## TODO

- Add a json config to add more configuration like extract only classes and relations or custom classes.
- Build steps using visual studio code
